// # Quantity

/*
Quantities are a datatype for physical measurements, which have a numerical value as well as a unit.
*/

// ## Prelude

#![cfg_attr(feature = "no-std", no_std)]
#![feature(nll)]

#[cfg(feature = "no-std")] extern crate rlibc;
#[cfg(feature="no-std")] #[macro_use] extern crate alloc;
#[cfg(not(feature = "no-std"))] extern crate core;

extern crate hashbrown;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate libm;
extern crate num;

#[cfg(feature = "no-std")] use alloc::vec::Vec;
#[cfg(feature = "no-std")] use alloc::fmt;
#[cfg(not(feature = "no-std"))] use core::fmt;
use hashbrown::hash_set::HashSet;

// ## Modules

mod quantities;

// ## Exported Modules

pub use self::quantities::{Quantity, ToQuantity, QuantityMath, make_quantity};